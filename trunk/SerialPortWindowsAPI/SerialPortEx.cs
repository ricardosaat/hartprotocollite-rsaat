﻿using System;
using System.ComponentModel;
using System.IO.Ports;
using System.Reflection;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;
using log4net;

namespace SerialPortWindowsAPI
{
    public class SerialPortEx : SerialPort
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(SerialPortEx));

        public SerialPortEx()
            : base()
        {
        }
        public SerialPortEx(string portName)
            : base(portName)
        {
        }
        public SerialPortEx(IContainer container)
            : base(container)
        {
        }
        public SerialPortEx(string portName, int baudRate)
            : base(portName, baudRate)
        {
        }
        public SerialPortEx(string portName, int baudRate, Parity parity)
            : base(portName, baudRate, parity)
        {
        }
        public SerialPortEx(string portName, int baudRate, Parity parity, int dataBits)
            : base(portName, baudRate, parity, dataBits)
        {
        }
        public SerialPortEx(string portName, int baudRate, Parity parity, int dataBits, StopBits stopBits)
            : base(portName, baudRate, parity, dataBits, stopBits)
        {
        }

        public void SetRtsControlToggle()
        {
            if (BaseStream == null)
                throw new InvalidOperationException("Cannot set RTS_CONTROL_TOGGLE until after the port has been opened.");
            SetDcbFlag(12, 3); // flag 12 is fRtsControl, value 3 is RTS_CONTROL_TOGGLE
            RtsControlToggleSet = true;
        }

        public void ClearRtsControlToggle()
        {
            if (BaseStream != null && RtsControlToggleSet)
            {
                SetDcbFlag(12, 0); // flag 12 is fRtsControl, value 0 is RTS_CONTROL_DISABLE
                RtsControlToggleSet = false;
            }
        }


        private bool RtsControlToggleSet = false;
        private SafeFileHandle _handle;

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool SetCommState(SafeFileHandle hFile, IntPtr lpDCB);

        private void SetDcbFlag(int flag, int value)
        {
            // Get the base stream and its type
            object baseStream = BaseStream;
            Type baseStreamType = baseStream.GetType();

            // Record current DCB Flag value so we can restore it on failure
            object originalDcbFlag = baseStreamType.GetMethod("GetDcbFlag", BindingFlags.NonPublic | BindingFlags.Instance)
              .Invoke(baseStream, new object[] { flag });

            // Invoke the private method SetDcbFlag to change flag to value
            baseStreamType.GetMethod("SetDcbFlag", BindingFlags.NonPublic | BindingFlags.Instance)
              .Invoke(baseStream, new object[] { flag, value });

            try
            {
                // Get the Win32 file handle for the port
                _handle = (SafeFileHandle)baseStreamType.GetField("_handle", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(baseStream);

                // Box the private DCB field
                object dcb = baseStreamType.GetField("dcb", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(baseStream);

                // Create unmanaged memory to copy dcb field into
                IntPtr hGlobal = Marshal.AllocHGlobal(Marshal.SizeOf(dcb));
                try
                {
                    // Copy dcb field to unmanaged memory
                    Marshal.StructureToPtr(dcb, hGlobal, false);

                    // Call SetCommState
                    if (!SetCommState(_handle, hGlobal))
                        throw new Win32Exception(Marshal.GetLastWin32Error());

                   
                }
                finally
                {
                    // Free the unmanaged memory
                    Marshal.FreeHGlobal(hGlobal);
                }
            }
            catch
            {
                // Restore original DCB Flag value if we failed to update the device
                baseStreamType.GetMethod("SetDcbFlag", BindingFlags.NonPublic | BindingFlags.Instance)
                  .Invoke(baseStream, new object[] { flag, originalDcbFlag });
                throw;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                ClearRtsControlToggle();

            base.Dispose(disposing);
        }
    }
}
