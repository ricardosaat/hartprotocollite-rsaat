﻿namespace Finaltec.Hart.Analyzer.ViewModel.MessageBox
{
    /// <summary>
    /// MessageBoxButtonType enum.
    /// </summary>
    public enum MessageBoxButtonType
    {
        Ok,
        OkCancel,
        YesNo
    }
}